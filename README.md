# CCQuery
## JQuery but like modern, and not gross.

### Functions

The $ function is an alias to the `document.querySelector` function.

The $$ function is an alias to the `document.querySelectorAll` function.

The $ function is an alias to the `document.querySelector` function.

The $_ function is an alias to the `document.getElementById` function, this is for individuals looking to query elements by ID in a more performant way.

### Added Methods

**.capitalize()**:
Capitalize the letter at the beggining of each word.

**.attr()**:
Gets or sets attributes
Usage:

`$('#exampleElement').attr('meta-pineapple')` - Gets the value of the `meta-pineapple` attribute.

`$('#exampleElement').attr('meta-pineapple', 'crunchy')` - Sets the value of the `meta-pineapple` attribute.


**.addClass()**:
Adds class to HTMLElement

**.removeClass()**:
Removes class to HTMLElement

**.hasClass()**:
Checks if an HTMLElement has a class and returns a boolean.

